# Synopsis: Set ClomRepairDelay on all hosts in cluster
#Rename to .ps1
# Download plink and put it in c:\temp http://the.earth.li/~sgtatham/putty/latest/x86/plink.exe
if ((Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null )
{
Add-PSSnapin VMware.VimAutomation.Core
Write-Host "Loading VMware PowerCLI Powershell plugin." 
}
else
{
Write-host "Checked for VMware PowerCLI Powershell plugin, already loaded."
}
$vCenter = Read-Host "Provide vCenter Server"
Connect-VIServer $vCenter -credential ( Get-Credential ) -WarningAction Silentlycontinue | Out-Null
$cluster = Read-Host "Enter VSAN Cluster name"


#
$hostpass = Read-Host "Please enter root password for the ESXi hosts"
$str1=’ECHO Y | /Applications/plink.dmg -pw $hostpass -l root ‘
$str2=’ esxcfg-advcfg -s 0 /VSAN/ClomEnableInplaceExpansion’
$outfile=’/tmp/report.txt'

    foreach($esxentry in (Get-VMHost|?{$_.Powerstate -eq “PoweredOn”})){
    $esxhost=”‘”+$esxentry.name+”‘”
    $command=$str1+$esxhost+$str2
    $esxentry.name >> $outfile
    $result=Invoke-Expression -Command $command
     foreach($resultline in 1..$result.length){
        $result[$resultline] >> $outfile
          }
      }
#
Write-host "Turning off in place expansion on all hosts.. Please wait"
sleep 10
Write-Host "Disconnected from $vcenter"
Disconnect-VIServer $vCenter -Confirm:$false
Write-Host "Script Complete"



